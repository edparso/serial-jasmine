/**
 * module serialReporter
 *
 */
'use strict';
var util = require("util");
var findSuite = (suiteName, obj)=> {
	let found = false;
	if (obj && obj.suites) {//root
		obj.suites.forEach((suite, key)=> {
			if (!found) {
				found = findSuite(suiteName, suite);
				if (found) {
					return found;
				}
			}
		})
	}
	if (obj.suite === suiteName) {
		found = obj;
		return obj;
	} else if (obj.nestedSuites) {
		obj.nestedSuites.forEach((suite, key)=> {
			if (!found) {
				if (suite.suite === suiteName) {
					found = suite;
					return found;
				} else {
					found = findSuite(suiteName, suite);
					if (found) {
						return found;
					}
				}
			}
		})
	}
	return found;
}

module.exports = ()=> {
	let level = 0;
	let currentSuite = null;
	let parentSuite = null;
	let out = {file: "xxx", suites: []};
	return {
		jasmineStarted: (suiteInfo)=> {
		},
		suiteStarted  : (result)=> {
			parentSuite = currentSuite;
			if (parentSuite) {
				let s = findSuite(parentSuite, out);
				s.nestedSuites.push( {
					suite       : result.description,
					parentSuite : parentSuite,
					suiteInfo   : result,
					nestedSuites: [],
					tests       : []
				});
			} else {
				out.suites.push( {
					suite       : result.description,
					parentSuite : parentSuite,
					suiteInfo   : result,
					nestedSuites: [],
					tests       : []
				})
			}
			currentSuite = result.description;
		},
		specStarted   : (result)=> {
			let s = findSuite(currentSuite, out);
			s.tests.push( {test: result.description, suite: s.suite, testInfo: result})
		},
		specDone      : (result)=> {
		},
		suiteDone     : (result)=> {
			currentSuite = parentSuite;
			let j;
			 j=out.suites.find((value,index,ar)=>value.suite===parentSuite);
			parentSuite = (j ) ? j : null;
			if (parentSuite) {
				parentSuite = parentSuite.suite;
			}
		},
		jasmineDone   : (result)=> {
			process.send({type: "jasmineDone", message: out});
		}

	}
}