/**
 * module index
 *
 */
'use strict';
var Promise = require("bluebird"),
	fs = Promise.promisifyAll(require("fs")),
	child_process = Promise.promisifyAll(require("child_process")),
	util = require("util"),
	argv = require("minimist")(process.argv),
	SPR = require("./lib/serialPromiseRunner"),
	minimist = require("minimist"),
	path = require("path"),
	colors = require("colors/safe");

var indent="\t";

var walkResults=(obj,theIndent,failures)=> {
	if (obj.file) {
		console.log(theIndent + "File: " + colors.bold(obj.file));
	}
	if (obj.suites) {
		obj.suites.forEach((suite, key)=>	{
			walkResults(suite, theIndent + indent,failures);
		})
	}
	if (obj.suite && !failures) {
		console.log(theIndent+"Suite: " + obj.suite);
	}
	if (obj.tests) {
		obj.tests.forEach((test, key) => {
			if(!failures){
				console.log(theIndent + indent + "Test: " + test.test + " Status: " + ((test.testInfo.status==="passed")?colors.green.bold(test.testInfo.status):colors.red.bold(test.testInfo.status)));
			}
			if (!(test.testInfo.status === "passed")) {
				if(failures) {
					if (obj.suite) {
						console.log(theIndent+"Suite: " + obj.suite);
					}
					console.log(theIndent + indent + "Test: " + test.test + " Status: " + ((test.testInfo.status === "passed") ? colors.green.bold(test.testInfo.status) : colors.red.bold(test.testInfo.status)));
				}
				test.testInfo.failedExpectations.forEach((item, index)=> {
					console.log(theIndent + indent + indent + "Reason: " + colors.red.bold(item.message));
					let fraction = "";
					if (item.stack) {
						let pos = item.stack.indexOf("at Object.");
						if (pos !== -1) {
							let endPoint = Math.min(pos + 100, item.stack.length);
							fraction = item.stack.substr(pos, endPoint - pos);
						} else {
							fraction = item.stack;
						}
						console.log(theIndent + indent + indent + "Stack: " + colors.red.bold(fraction));
					}
				})
			}
		})
	}
	if (obj.nestedSuites) {
		obj.nestedSuites.forEach((suite, key)=> {
			walkResults(suite, theIndent + indent,failures);
		})
	}
}
module.exports.runTasks = (theTasks, theReporter, saveData,silent,failures)=> new Promise((resolve, reject)=> {
	let theArgs = minimist(process.argv.slice(2));
	theReporter = theReporter ? theReporter : (theArgs.reporter ? path.resolve(theArgs.reporter) : void(0));
	if (!theReporter) {
		theReporter = void(0);
	}
	theTasks = theTasks || theArgs._;
	if (saveData === void(0)) {
		if (theArgs.sj !== void(0)) {
			if (typeof theArgs.sj === 'boolean') {
				saveData = theArgs.sj;
			} else if (typeof theArgs.sj === 'string') {
				saveData = (theArgs.sj.toLowerCase() === 'true') || (theArgs.sj.toLowerCase() === 't') || (theArgs.sj.toLowerCase() === 'yes') || (theArgs.sj.toLowerCase() === 'y')
			}
		}
		if (saveData === void(0)) {
			saveData = false;
		}
	}
	let tasks = [];
	let results = void(0);
	theTasks.forEach((test)=> {
		tasks.push({
			name : test,
			args : [test, theReporter, saveData],
			funct: require("./lib/runFork")
		});
	});
	let spr = new SPR(tasks);
	spr.runTasks()
		.then((res)=> {
			results = res;
			if(!silent) {
				if (saveData && !theReporter) {
					Object.keys(results).forEach((key)=>walkResults(results[key].results, "",failures));
				}
			}
			resolve(res);
		})
		.catch(reject)
		.finally(()=>spr = void(0));
})

if ((argv._.length > 1) && argv._[1].indexOf("index.js") !== -1) {
	module.exports.runTasks()
		.then((results)=> {
		})
		.catch((err)=>console.log(err, err.stack));
}