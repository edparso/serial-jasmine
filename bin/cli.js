#!/usr/bin/env node

var argv=require("minimist")(process.argv.splice(2));
var tasks=argv._;
var reporter=argv.reporter;
var savedata=argv.sj;
var sh=require("../index.js");
var silent=argv.silent;
var failures=argv.failures;
sh.runTasks(tasks,reporter,savedata,silent,failures)
	.then(function(results){
    process.exit(0);
  })
	.catch(function(error){
    console.log(error,error.stack)
    process.exit(1);
  });

